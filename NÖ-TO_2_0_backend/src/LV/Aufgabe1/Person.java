package LV.Aufgabe1;

import java.awt.HeadlessException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JOptionPane;

public class Person {
	
	static ArrayList<Person> allPerson = new ArrayList<>();
	static ArrayList<Person> winner = new ArrayList <>();
	static ArrayList<String> allWinners = new ArrayList <> ();
	//static File winnerFile = null;
	//static BufferedWriter bw = new BufferedWriter();
	
	
	String id;
	String firstName;
	String secondName;
	
	
	
	public Person(String id, String firstName, String secondName) {
		this.id = id;
		this.firstName = firstName;
		this.secondName = secondName;
	}


	@Override
	public String toString() {
		return "Person [" + (id != null ? "id=" + id + ", " : "")
				+ (firstName != null ? "firstName=" + firstName + ", " : "")
				+ (secondName != null ? "secondName=" + secondName : "") + "]";
	}
	
	public static void game() throws IOException {
		
		if (allPerson.size()< 3) {
			for (int i = 0; i < 3; i++) {
				int randomId = ThreadLocalRandom.current().nextInt(0, allPerson.size());
				Person p = allPerson.get(randomId);
				winner.add(p);
				if (allPerson.size()!= 1) {
					allPerson.remove(p);
				}
			}
			JOptionPane.showMessageDialog(null, "Last Man Standing: "+allPerson.get(0).firstName+ " "+ allPerson.get(0).secondName);
		}
		else {
			for (int i = 0; i < 3; i++) {
			int randomId = ThreadLocalRandom.current().nextInt(0, allPerson.size());
			Person p = allPerson.get(randomId);
			winner.add(p);
			allPerson.remove(p);
			}	
			
			JOptionPane.showMessageDialog(null, "1.Platz: "+ winner.get(0).firstName+" "+winner.get(0).secondName+
					System.lineSeparator() + "2.Platz: "+winner.get(1).firstName+" "+winner.get(1).secondName +
					System.lineSeparator() + "3.Platz: "+winner.get(2).firstName+" "+winner.get(2).secondName);
			Person.toTxt(winner);
			
			winner.clear();
			}
			
		
		
		return;
		
	}
	
	public static String toTxt(ArrayList<Person> winner) {
		
		String personData;
		
		for (int i = 0; i < winner.size(); i++) {
			personData= winner.get(i).id+";"+winner.get(i).firstName+";"+winner.get(i).secondName;
			allWinners.add(personData);
		}
		allWinners.forEach(System.out::println);
		
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter("winnerFile.txt"));
		
			for (String gameWinners : allWinners) {
				writer.write(gameWinners +";");
				writer.write(System.lineSeparator()); 
				
			}
			writer.close();
			JOptionPane.showMessageDialog(null,"Successfully written to file" );
		} catch (HeadlessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "winnerFile.txt";
	}

	public static void lastManStanding () {
		int number = (int)Math.ceil(allPerson.size()/3.0);
		if (allPerson.size()%3 == 0) {
			JOptionPane.showMessageDialog(null,"no last man standing possible" );
		}
		else {
			for (int i = 0; i < number; i++) {
				try {
					Person.game();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static ArrayList<Person> readFile() throws FileNotFoundException {
		
		File file = new File ("Mappe 7.csv");
		
		try (Scanner scan = new Scanner(file)) {

			while (scan.hasNext()) {
	
				String[] parts = scan.nextLine().split(";");
	
				String id = parts[0];
				String firstName = parts[1];
				String secondName = parts[2];
	
				Person p = new Person(id, firstName, secondName);
				allPerson.add(p);
			}

		} catch (Exception e) {
			System.err.println("ERROR: " + e.getLocalizedMessage());
		}
		allPerson.forEach(System.out::println);
		return allPerson;
	}
	
	
	
}
